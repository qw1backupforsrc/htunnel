// Copyright (C) 2017 Michał Matczuk
// Use of this source code is governed by an AGPL-style
// license that can be found in the LICENSE file.

package tunnel

import (
	"bufio"
	"context"
    "crypto/tls"
    "crypto/x509"
    //"encoding/base64"
    //"encoding/json"
    "fmt"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path"
    "strings"
    "time"

    "gitlab.com/qw1backupforsrc/htunnel/go-http-tunnel/log"
    "gitlab.com/qw1backupforsrc/htunnel/go-http-tunnel/proto"
)

// HTTPProxy forwards HTTP traffic.
type HTTPProxy struct {
	httputil.ReverseProxy
	// localURL specifies default base URL of local service.
	localURL *url.URL
	// localURLMap specifies mapping from ControlMessage.ForwardedHost to
	// local service URL, keys may contain host and port, only host or
	// only port. The order of precedence is the following
	// * host and port
	// * port
	// * host
	localURLMap map[string]*url.URL
	// logger is the proxy logger.
	logger log.Logger
}

// NewHTTPProxy creates a new direct HTTPProxy, everything will be proxied
// to localURL.
func NewHTTPProxy(localURL *url.URL, logger log.Logger) *HTTPProxy {
	if logger == nil {
		logger = log.NewNopLogger()
	}

	p := &HTTPProxy{
		localURL: localURL,
		logger:   logger,
	}
	p.ReverseProxy.Director = p.Director
	p.ReverseProxy.FlushInterval = 100 * time.Millisecond

	return p
}

// NewMultiHTTPProxy creates a new dispatching HTTPProxy, requests may go to
// different backends based on localURLMap.
func NewMultiHTTPProxy(localURLMap map[string]*url.URL, logger log.Logger, rootCAs *x509.CertPool) *HTTPProxy {
	if logger == nil {
		logger = log.NewNopLogger()
	}

	p := &HTTPProxy{
		localURLMap: localURLMap,
		logger:      logger,
	}
	p.ReverseProxy.Director = p.Director

    fmt.Println("local root CAs:", len(rootCAs.Subjects()))
	config := &tls.Config{
		RootCAs: rootCAs,
	}
	tr := &http.Transport{TLSClientConfig: config}

	p.ReverseProxy.Transport = tr
	p.ReverseProxy.Director = p.Director


	return p
}

// Proxy is a ProxyFunc.
func (p *HTTPProxy) Proxy(w io.Writer, r io.ReadCloser, msg *proto.ControlMessage) {
	switch msg.ForwardedProto {
	case proto.HTTP, proto.HTTPS:
		// ok
	default:
		p.logger.Log(
			"level", 0,
			"msg", "unsupported protocol",
			"ctrlMsg", msg,
		)
		return
	}

	rw, ok := w.(http.ResponseWriter)
	if !ok {
		p.logger.Log(
			"level", 0,
			"msg", "expected http.ResponseWriter",
			"ctrlMsg", msg,
		)
	}

	req, err := http.ReadRequest(bufio.NewReader(r))
	if err != nil {
		p.logger.Log(
			"level", 0,
			"msg", "failed to read request",
			"ctrlMsg", msg,
			"err", err,
		)
		return
	}

	setXForwardedFor(req.Header, msg.RemoteAddr)
	req.URL.Host = msg.ForwardedHost

	p.ServeHTTP(rw, req)
}

// Director is ReverseProxy Director it changes request URL so that the request
// is correctly routed based on localURL and localURLMap. If no URL can be found
// the request is canceled.
func (p *HTTPProxy) Director(req *http.Request) {
	orig := req.URL

	target := p.localURLFor(req)
	if target == nil {
		p.logger.Log(
			"level", 1,
			"msg", "no target",
			"url", req.URL,
		)

		_, cancel := context.WithCancel(req.Context())
		cancel()

        fmt.Println("wwwwww3 connection canceled")
		return
	}

	req.URL.Host = target.Host
	req.URL.Scheme = target.Scheme
	req.URL.Path = singleJoiningSlash(target.Path, req.URL.Path)

	targetQuery := target.RawQuery
	if targetQuery == "" || req.URL.RawQuery == "" {
		req.URL.RawQuery = targetQuery + req.URL.RawQuery
	} else {
		req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
	}
	if _, ok := req.Header["User-Agent"]; !ok {
		// explicitly disable User-Agent so it's not set to default value
		req.Header.Set("User-Agent", "")
	}

	req.Host = req.URL.Host

	p.logger.Log(
		"level", 2,
		"action", "url rewrite",
		"from", &orig,
		"to", req.URL,
	)
}

func singleJoiningSlash(a, b string) string {
	if a == "" || a == "/" {
		return b
	}
	if b == "" || b == "/" {
		return a
	}

	return path.Join(a, b)
}

const (
    ch47 = byte('/')
    max1stTime = 60 // time allow between JWT creation to init reach client
)

func (p *HTTPProxy) getJwtPayload(r *http.Request) (*Jpayload, bool) {

    _, state, _ := removeRandState(false, r)
    jc, err := r.Cookie(state)
    if err != nil {
		return nil, false
    }
    return getJwtPayload((*jc).Value)
}

func getPath1(p string) (string, bool) {
    if p[0] != ch47 {   // not start with "/"
        return "", false
    }

    ok := true
    if len(p) > 1 {
        i1 := strings.Index(p[1:], "/") + 1
        i2 := strings.Index(p[1:], "?") + 1
        if i1 > 0 {
            if i2 > 0 {
               if i1 < i2 {
                    return p[:i1], ok
               } else {
                    return p[:i2], ok
               }
            } else {
                return p[:i1], ok
            }
        } else {
            if i2 > 0 {
                return p[:i2], ok
            }
        }
    }
    return p, ok
}

func checkIdleTime(jpl *Jpayload) bool {

    t, exp := isExpired(jpl.Jti)
    if !exp {
        return true
    }
    if t > 0 {   // not first time
        deleteIdleMap(jpl.Jti)
        return false
    }

    // first time
    if currTime + 60 > jpl.Jit + max1stTime {
        return false
    } else {
        return true
    }
}

func (p *HTTPProxy) localURLFor(req *http.Request) *url.URL {

	if len(p.localURLMap) == 0 {
		return p.localURL
	}

    jpl, ok := p.getJwtPayload(req)
    if !ok || !checkIdleTime(jpl) {
        if ok {
            fmt.Println("=====> idle timeout")
        }
		return nil
    }
    updateIdleMap(jpl.Jti)    // update idle time

    path1, ok := getPath1(jpl.Aud)
    if !ok {
		return p.localURL
    }

	if addr := p.localURLMap[path1]; addr != nil {
		return addr
	}

	// try host and port
	u := req.URL
	hostPort := u.Host
	if addr := p.localURLMap[hostPort]; addr != nil {
		return addr
	}

	// try port
	host, port, _ := net.SplitHostPort(hostPort)
	if addr := p.localURLMap[port]; addr != nil {
		return addr
	}

	// try host
	if addr := p.localURLMap[host]; addr != nil {
		return addr
	}

	return p.localURL
}
