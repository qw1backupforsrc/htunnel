package tunnel

import (
    "sync"
    "time"
)

const (
    checkInterval = 60 * time.Second
)

var sess map[string]int
var currTime, expTime int
var mut1 = &sync.Mutex{}

func updateIdleMap(name string) {
    mut1.Lock()
    sess[name] = expTime
    mut1.Unlock()
}

func readIdleMap(name string) int {
    mut1.Lock()
    t, _ := sess[name]
    mut1.Unlock()
    return t
}

func isExpired(name string) (int, bool) {
    mut1.Lock()
    t, _ := sess[name]
    mut1.Unlock()
    return t, t < currTime
}

func deleteIdleMap(name string) {
    mut1.Lock()
    delete(sess, name)
    mut1.Unlock()
}

func purgeMap() {
    old := make([]string, len(sess))
    mut1.Lock()
    for k, v := range sess {
        if v < currTime {   // idle too long
            old = append(old, k)
        }
    }
    for _, k := range old {
        delete(sess, k)
    }
    mut1.Unlock()

}

func startIdleMonitor(itime int) {

    sess = make(map[string]int)
    currTime = int(time.Now().Unix())
    expTime = currTime + itime

    for {
        time.Sleep(checkInterval)
        go purgeMap()
        currTime = int(time.Now().Unix())
        expTime = currTime + itime
    }
}
