package tunnel

import (
    "bytes"
	"crypto/tls"
	"crypto/x509"
    "encoding/pem"
	"fmt"
	"io/ioutil"
	"strings"
)

func LoadX509KeyPair(certFile, keyFile string, passwd []byte) (tls.Certificate, error) {

    cert := tls.Certificate{}
    certPEMBlock, err := ioutil.ReadFile(certFile)
    if err != nil {
      return cert, err
    }
    keyPEMBlock, err := ioutil.ReadFile(keyFile)
    if err != nil {
      return cert, err
    }
  
    var keyDERBlock *pem.Block
    for {
        keyDERBlock, keyPEMBlock = pem.Decode(keyPEMBlock)
        if keyDERBlock == nil {
          panic("crypto/tls: failed to parse key PEM data")
          return cert, nil
        }
  
  	  if x509.IsEncryptedPEMBlock(keyDERBlock) {
           out, err := x509.DecryptPEMBlock(keyDERBlock, passwd)
  	     if err != nil {
               fmt.Println("password error")
  	         return cert, err
  	    }
          keyDERBlock.Bytes = out
          var buf bytes.Buffer
          if err := pem.Encode(&buf, keyDERBlock); err != nil {
              panic(err)
          }
  
          return tls.X509KeyPair(certPEMBlock, buf.Bytes())
      } else {
           fmt.Println("not a pw block")
      }
      if keyDERBlock.Type == "PRIVATE KEY" || strings.HasSuffix(keyDERBlock.Type, " PRIVATE KEY") {
          break
      }
    }
  
    return cert, nil
}

func GetCertPass(halfpwd string) []byte {
    return []byte(halfpwd)
}
