package tunnel

import (
    "bytes"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
    "io/ioutil"
    "strings"
    "time"
	"errors"
)

const PS256 = true
var Jhead = base64.URLEncoding.EncodeToString([]byte(`{"alg":"PS256", "typ": "JWT"}`))
type jwtVerifier struct {
    *rsa.PublicKey
}

type Jpayload struct {
   Jit int    `json:"jit"`
   Exp int64  `json:"exp"`
   Aud string `json:"aud"`
   Sub string `json:"sub"`
   Jti string `json:jti`
}

func (r * jwtVerifier) ChkJwt(jwt, state, alg, u string) bool {
    f := strings.Split(jwt, ".")
    if len(f) != 3 {
        return false
    }

    // check sub, state, and exp time
    m, _ := base64.URLEncoding.DecodeString(f[1])
    var p Jpayload
    err := json.Unmarshal(m, &p)
    if err != nil || p.Sub != u+p.Aud || p.Exp < time.Now().Unix() || p.Jti != state {
        return false
    }

    // check alg
    m, _ = base64.URLEncoding.DecodeString(f[0])
    if !bytes.Contains(m, []byte(alg)) {
        return false
    }

    s, _ := base64.URLEncoding.DecodeString(f[2])
    err = r.jwtVerify([]byte(f[0] + "." + f[1]), s)
    if err == nil {
        return true
    } else {
        return false
    }
}

// jwtp: header + payload, jwts: signature
func (r * jwtVerifier) ChkJwt2(jwtp, jwts string, alg, sub string) bool {
    f := strings.Split(jwtp, ".")
    if len(f) != 2 {
        return false
    }

    // check sub and exp time
    m, _ := base64.URLEncoding.DecodeString(f[1])
    var p Jpayload
    json.Unmarshal(m, &p)
    if p.Sub != sub || p.Exp < time.Now().Unix() {
        return false
    }

    // check alg
    m, _ = base64.URLEncoding.DecodeString(f[0])
    if !bytes.Contains(m, []byte(alg)) {
        return false
    }

    s, _ := base64.URLEncoding.DecodeString(jwts)
    err := r.jwtVerify([]byte(jwtp), s)
    if err == nil {
        return true
    } else {
        return false
    }
}

func (r *jwtVerifier) jwtVerify(data, sig []byte) error {

    if !PS256 { // use RSASSA-PKCS1-v1_5 with SHA-256, JWT alg RS256

	    h := sha256.New()
	    h.Write(data)
	    d := h.Sum(nil)
	    return rsa.VerifyPKCS1v15(r.PublicKey, crypto.SHA256, d, sig)

    } else {  // use RSASSA-PSS with SHA-256 and MGF1 with SHA-256, JWT alg PS256

	    //Verify Signature
	    var opts rsa.PSSOptions
	    opts.SaltLength = rsa.PSSSaltLengthAuto // for simple example
	    newhash := crypto.SHA256
	    pssh := newhash.New()
	    pssh.Write(data)
	    hashed := pssh.Sum(nil)
	    return rsa.VerifyPSS(r.PublicKey, newhash, hashed, sig, &opts)

    }
}

func NewJwtVerifier(pemfile string) *jwtVerifier {
    o, err := loadKey(pemfile, true)
    if err != nil {
        panic(err)
    }
	return &jwtVerifier{o.(*rsa.PublicKey)}
}

func readPemFile(fname string) []byte {
    key, err := ioutil.ReadFile(fname)
    if err != nil {
        panic(err)
    }
    return key
}

// parsePublicKey parses a PEM encoded private key.
func loadKey(pemfile string, publicKey bool) (interface{}, error) {
    keybytes := readPemFile(pemfile)
	block, _ := pem.Decode(keybytes)
	if block == nil {
		return nil, errors.New("ssh: no key found")
	}

    if publicKey && (block.Type == "PUBLIC KEY" || block.Type == "RSA PUBLIC KEY") {
		return x509.ParsePKIXPublicKey(block.Bytes)
    } else if !publicKey && (block.Type == "PRIVATE KEY" || block.Type == "RSA PRIVATE KEY") {
		return x509.ParsePKCS1PrivateKey(block.Bytes)
	} else {
	    return nil, errors.New("wrong key type")
    }
}

func getJwtPayload(v string) (*Jpayload, bool) {

    if len(v) == 0 {
		return nil, false
    }
    e := strings.Split(v, ".")
    if len(e) != 3 {
		return nil, false
    }
    b, err := base64.URLEncoding.DecodeString(e[1])
    if err != nil {
		return nil, false
    }

    jpl := new(Jpayload)
    err = json.Unmarshal(b, jpl)
    if err != nil {
		return nil, false
    }
	return jpl, true
}


