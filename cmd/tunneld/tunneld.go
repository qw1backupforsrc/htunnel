// Copyright (C) 2017 Michał Matczuk
// Use of this source code is governed by an AGPL-style
// license that can be found in the LICENSE file.

package main

import (
	"crypto/tls"
	"crypto/x509"
    "encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"golang.org/x/net/http2"

    "gitlab.com/qw1backupforsrc/htunnel/go-http-tunnel"
    "gitlab.com/qw1backupforsrc/htunnel/go-http-tunnel/id"
    "gitlab.com/qw1backupforsrc/htunnel/go-http-tunnel/log"
)

var cfgfile = "optCfg.json"

func readOptions(fn string) (*options, map[string]interface{}, error) {
    jf, err := os.Open(fn)
    if err != nil {
	    return nil, nil, err
    }
    defer jf.Close()

    b, _ := ioutil.ReadAll(jf)
    m := make(map[string]interface{})
    err = json.Unmarshal(b, &m)
    if err != nil {
	    return nil, nil, err
    }

    var o options
	o.httpAddr, _ = m["httpAddr"].(string)
	delete(m, "httpAddr")
	o.httpsAddr, _ = m["httpsAddr"].(string)
	delete(m, "httpsAddr")
	o.tunnelAddr, _ = m["tunnelAddr"].(string)
	delete(m, "tunnelAddr")
	o.tlsCrt, _ = m["tlsCrt"].(string)
	delete(m, "tlsCrt")
	o.tlsKey, _ = m["tlsKey"].(string)
	delete(m, "tlsKey")
	o.rootCA, _ = m["rootCA"].(string)
	delete(m, "rootCA")
	o.clients, _ = m["clients"].(string)
	delete(m, "clients")
	o.logLevel, _ = m["logLevel"].(int)
	delete(m, "logLevel")
	o.version, _ = m["version"].(bool)
	delete(m, "version")

    return &o, m, nil
}

func main() {

    pubKey := "pubkey.pem"

    var opts *options
    var params2 map[string]interface{}
    if len(os.Args) == 2 {
        cfgfile = os.Args[1]
    }
    _, err := os.Stat(cfgfile)
    var halfpwd string
    if err == nil {
        opts, params2, err = readOptions(cfgfile)
        if err == nil {
            s, ok := params2["jwtPubKey"]
            if ok {
                pubKey = s.(string)
            }
            s, ok = params2["loginServer"]
            if ok {
                tunnel.LoginServer = s.(string)
            }
            s, ok = params2["passwd.half"]
            if ok {
                halfpwd = s.(string)
            }
        }
    }
    if err != nil {
	    opts = parseArgs()
    }
    tunnel.TssPort = opts.httpsAddr
    tunnel.JwtChecker = tunnel.NewJwtVerifier(pubKey)

	if opts.version {
		fmt.Println(version)
		return
	}

	fmt.Println(banner)

	logger := log.NewFilterLogger(log.NewStdLogger(), opts.logLevel)

	tlsconf, err := tlsConfig(opts, halfpwd)
	if err != nil {
		fatal("failed to configure tls: %s", err)
	}

	autoSubscribe := opts.clients == ""

	// setup server
	server, err := tunnel.NewServer(&tunnel.ServerConfig{
		Addr:          opts.tunnelAddr,
		AutoSubscribe: autoSubscribe,
		TLSConfig:     tlsconf,
		Logger:        logger,
	})
	if err != nil {
		fatal("failed to create server: %s", err)
	}

    tlsconf2 := &tls.Config{Certificates: []tls.Certificate{tlsconf.Certificates[0]}, MinVersion: tls.VersionTLS12}

	if !autoSubscribe {
		for _, c := range strings.Split(opts.clients, ",") {
			if c == "" {
				fatal("empty client id")
			}
			identifier := id.ID{}
			err := identifier.UnmarshalText([]byte(c))
			if err != nil {
				fatal("invalid identifier %q: %s", c, err)
			}
			server.Subscribe(identifier)
		}
	}

	// start HTTP
	if opts.httpAddr != "" {
		go func() {
			logger.Log(
				"level", 1,
				"action", "start http",
				"addr", opts.httpAddr,
			)

			fatal("failed to start HTTP: %s", http.ListenAndServe(opts.httpAddr, server))
		}()
	}

	// start HTTPS
	if opts.httpsAddr != "" {
		go func() {
			logger.Log(
				"level", 1,
				"action", "start https",
				"addr", opts.httpsAddr,
			)

			s := &http.Server{
				Addr:    opts.httpsAddr,
				Handler: server,
                TLSConfig: tlsconf2,
			}
			http2.ConfigureServer(s, nil)

			fatal("failed to start HTTPS: %s", s.ListenAndServeTLS("", ""))
		}()
	}

	server.Start()
}

func tlsConfig(opts *options, halfpwd string) (*tls.Config, error) {
	// load certs
	cert, err := tunnel.LoadX509KeyPair(opts.tlsCrt, opts.tlsKey, tunnel.GetCertPass(halfpwd))
	if err != nil {
		return nil, err
	}

	// load root CA for client authentication
	clientAuth := tls.RequireAnyClientCert
	var roots *x509.CertPool
	if opts.rootCA != "" {
		roots = x509.NewCertPool()
		rootPEM, err := ioutil.ReadFile(opts.rootCA)
		if err != nil {
		    return nil, err
		}
		if ok := roots.AppendCertsFromPEM(rootPEM); !ok {
		    return nil, err
		}
		clientAuth = tls.RequireAndVerifyClientCert
	}

	return &tls.Config{
		Certificates:             []tls.Certificate{cert},
		ClientAuth:               clientAuth,
		ClientCAs:                roots,
		SessionTicketsDisabled:   true,
		MinVersion:               tls.VersionTLS12,
		CipherSuites:             []uint16{tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256},
		PreferServerCipherSuites: true,
		NextProtos:               []string{"h2"},
	}, nil
}

func fatal(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
	fmt.Fprint(os.Stderr, "\n")
	os.Exit(1)
}
