module gitlab.com/qw1backupforsrc/htunnel

go 1.13

require (
	github.com/calmh/luhn v2.0.0+incompatible
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/felixge/tcpkeepalive v0.0.0-20160804073959-5bb0b2dea91e
	github.com/golang/mock v1.3.1
	golang.org/x/net v0.0.0-20191112182307-2180aed22343
	gopkg.in/yaml.v2 v2.2.5
)
